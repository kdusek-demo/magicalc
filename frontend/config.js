const api_config = function(){
  console.log('API HOST: ',process.env.NEXT_PUBLIC_API_URL);
  return process.env.NEXT_PUBLIC_API_URL;
}();

const GCP_KEY = null;
const AWS_KEY = null;
const AWS_SECRET = null;

export {
  api_config, AWS_KEY, GCP_KEY, AWS_SECRET
}
